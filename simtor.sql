-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2015 at 02:21 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simtor`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `role`) VALUES
(1, 'ichsan', '81dc9bdb52d04dc20036dbd8313ed055', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `courier`
--

CREATE TABLE IF NOT EXISTS `courier` (
  `id_courier` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id_courier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `courier`
--

INSERT INTO `courier` (`id_courier`, `username`, `password`) VALUES
(1, 'Hendra', '1234'),
(2, 'Acong', 'asd'),
(5, 'Dadang', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
  `id_goods` int(10) NOT NULL AUTO_INCREMENT,
  `track_number` varchar(255) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `goods` varchar(100) NOT NULL,
  `phone` int(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` enum('on the way','sent') NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `waypoint` text NOT NULL,
  `id_courier` int(10) NOT NULL,
  PRIMARY KEY (`id_goods`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `goods`
--

INSERT INTO `goods` (`id_goods`, `track_number`, `sender`, `receiver`, `goods`, `phone`, `address`, `status`, `latitude`, `longitude`, `waypoint`, `id_courier`) VALUES
(5, '', 'faisal', '', 'Pakaian', 89302943, 'Jalan Jendral Sudirman, Terban, Special Region of Yogyakarta, Indonesia', 'on the way', '-7.782956199999999', '110.37183440000001', '', 3),
(6, '', 'ari', '', 'hhay', 37432, 'Jalan Gejayan, Catur Tunggal, Special Region of Yogyakarta, Indonesia', 'on the way', '-7.7716333', '110.38994279999997', '', 2),
(21, 'qwerty12345', 'Ichsan', 'Wahyudi', 'Baju', 2147483647, 'Perumahan Kaliurang Pratama, 8 5 Jalan Kaliurang KM, Sinduharjo, Special Region of Yogyakarta, Indonesia', 'on the way', '-7.733147788366001', '110.39287805557251', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE IF NOT EXISTS `pengiriman` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `telepon` int(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `id_kurir` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
