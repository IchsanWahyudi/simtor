 <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_kurir', '', TRUE);
		$this->load->model('m_barang', '', TRUE);
	}

	public function index() {
		if ($this->session->userdata('loginkurir')) {
			$id = $this->session->userdata('loginkurir')['id_courier'];
			$data = array(
				'pagetitle'		=> "SIMTOR Kurir",
				'pos_parent'	=> "home",
				'title'			=> "Dashboard",
				'subtitle'		=> "This is your dashboard",
				'content'		=> 'content/kurir/home',
				'css'			=> 'style-kurir-home.css',
				'data'			=> $this->m_barang->getBarangKurir($id),
			);
			$this->load->view('content/template/page', $data);
		} else {
			redirect(base_url().'kurir/login', 'refresh');
		}
	}

	function status($id='',$stat=''){
		if ($this->session->userdata('loggedin')) {

			 $data = array(
					'status'	=>  $stat
				);

			 $this->m_barang->update($id,$data);	
			 redirect(base_url().'kurir', 'refresh');

		} else {
			redirect(base_url().'admin/login', 'refresh');
		}
	}

}