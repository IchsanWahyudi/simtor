<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('m_kurir', '', TRUE);
    }

    public function index() {
        if ($this->session->userdata('loginkurir')) {
            redirect('kurir', 'refresh');
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('content/kurir/login');
            } else {
                redirect('kurir', 'refresh');
            }
        } 
    }

    public function check_database($password) {
        $username = $this->input->post('username');

        $result = $this->m_kurir->login($username, $password);

        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                    'id_courier' => $row->id_courier,
                    'username' => $row->username,
                );
                $this->session->set_userdata('loginkurir', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Username and Password is not Valid!');
            return false;
        }
    }

    function logout() {
        $this->session->keep_flashdata('pesan_logout');
        $this->session->unset_userdata('loginkurir');
        redirect('kurir', 'refresh');
    }

}

?>
