<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Barang extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_kurir');
		$this->load->model('m_barang');
		$this->load->helper(array('form'));
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		if ($this->session->userdata('loggedin')) {
			$data = array(
				'pagetitle'   => "Sistem Monitoring Pengiriman Barang - Dashboard",
				'pos_parent'  => "barang",
				'pos_child'   => "daftar",
				'title'       => "Manajemen Barang",
				'subtitle'    => "Manajemen Barang",
				'breadcrumb'  =>  array('Barang','Daftar Pengiriman Barang'),
				'data'        =>  $this->m_barang->join(),
				'content'     => 'admin/barang/daftar',
			);
			$this->load->view('admin/template/page', $data);
		} else {
			redirect(base_url().'admin/login', 'refresh');
		}
	}

	public function tambah() {
		if ($this->session->userdata('loggedin')) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("class='form-error' title='", "'");
			$this->form_validation->set_message('is_unique', 'Permalink already exist.');
			$this->form_validation->set_rules('nomor_pengiriman', 'Nomor_pengiriman', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama_pengirim', 'Nama_pengirim', 'trim|required|xss_clean');
			$this->form_validation->set_rules('nama_penerima', 'Nama_penerima', 'trim|required|xss_clean');
			$this->form_validation->set_rules('barang', 'Barang', 'trim|required|xss_clean');
			$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required|xss_clean');
			$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
			$this->form_validation->set_rules('lat', 'Koordinat latitude', 'trim|required|xss_clean');
			$this->form_validation->set_rules('lng', 'Koordinat longitude', 'trim|required|xss_clean');
			$data_brg = $this->m_barang->getAll();
			if ($this->form_validation->run() == FALSE) {
				$data = array (
				  'pagetitle'   =>  "Sistem Monitoring Pengiriman Barang - Barang",
				  'pos_parent'  =>  "barang",
				  'pos_child'   =>  "tambah",
				  'title'       =>  "Manajemen Barang",
				  'data_kurir'  =>  $this->m_kurir->getAll(),
				  'subtitle'    =>  "",
				  'breadcrumb'  =>  array('Barang','Tambah'),
				  'content'     =>  'admin/barang/tambah'
				);

				$this->load->view('admin/template/page', $data);

			}else {
				
				$data = array(
						'track_number'  => $this->input->post('nomor_pengiriman'),
						'sender'        => $this->input->post('nama_pengirim'),
						'receiver'      => $this->input->post('nama_penerima'),
						'goods'         => $this->input->post('barang'),
						'phone'         => $this->input->post('telepon'),
						'address'       => $this->input->post('alamat'),
						'latitude'      => $this->input->post('lat'),
						'longitude'     => $this->input->post('lng'),
						'id_courier'    => $this->input->post('id_kurir'),
					);

				foreach ($data_brg as $b) {
					if ($this->input->post('nomor_pengiriman') == $b->track_number) {
						$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
							<p>Nomor pengiriman sudah digunakan<i class='fa fa-times'></i></p>
						</div>");
					redirect(base_url().'admin/barang/tambah','refresh');
					}
					if ($this->input->post('id_kurir') == $b->id_courier) {
						$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
							<p>Kurir sudah ada pengiriman, silahkan pilih kurir yang lain<i class='fa fa-times'></i></p>
						</div>");
						redirect(base_url().'admin/barang/tambah','refresh');
					}
				}

				if ($this->input->post('id_kurir') == 0) {
					$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
							<p>Anda belum memilih kurir, Silahkan pilih kurir terlebih dahulu<i class='fa fa-times'></i></p>
						</div>");
					redirect(base_url().'admin/barang/tambah','refresh');
				}


				$id_goods = $this->m_barang->insert($data);
				$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
					<p>Silahkan Tentukan Rute Pengiriman Barang<i class='fa fa-times'></i></p>
				</div>");
				redirect(base_url().'admin/barang/pilih_rute/'.$id_goods, 'refresh');
			}
		} else {
			//If no session, redirect to login page
			redirect(base_url().'admin/login', 'refresh');
		}
	}

	 public function pilih_rute($id='') {
		if ($this->session->userdata('loggedin')) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("class='form-error' title='", "'");
			$this->form_validation->set_message('is_unique', 'Permalink already exist.');
			$this->form_validation->set_rules('fix_waypoint[]', 'Waypoint', 'trim|required|xss_clean');
			
			if ($this->form_validation->run() == FALSE) {
				$data = array (
					'pagetitle'		=>	"Sistem Monitoring Pengiriman Barang - Kurir",
					'pos_parent'	=>	"barang",
					'pos_child'		=>	"tambah",
					'title'			=>	"Manajemen Rute Terpendek",
					'subtitle'		=>	"Pencarian Rute",
					'breadcrumb'	=>	array('barang','pencarian rute'),
					'content'		=>	'admin/barang/pilih_rute',
					'data'			=>	$this->m_barang->getDetail($id),
				);
				// print_r($data);
				$this->load->view('admin/template/page', $data);

			}else {
				
				$data = array(
					'waypoint'	=>	implode('|',$this->input->post('fix_waypoint')),
				);
				
				// print_r($data);
				// die();
				$this->m_barang->insert_way($id, $data);
				redirect(base_url().'admin/barang/', 'refresh');
			}
		} else {
			//If no session, redirect to login page
			redirect(base_url().'admin/login', 'refresh');
		}
	}


	function hapus($id=''){
		if ($this->session->userdata('loggedin')) {
			$this->m_barang->delete($id);

			$this->session->set_flashdata("pesan", "<div class='alert alert-error'>
				<p><b>Success!</b> Data berhasil dihapus dari database.<i class='fa fa-times'></i></p>
			</div>");
			redirect(base_url().'admin/barang', 'refresh');
		} else {
			//If no session, redirect to login page
			redirect(base_url().'admin/login', 'refresh');
		}
	}

}