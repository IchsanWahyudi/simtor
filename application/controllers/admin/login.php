<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper(array('form'));
		$this->load->model('m_admin', '', TRUE);
	}

	public function index() {
		if ($this->session->userdata('loggedin')) {
			redirect('admin', 'refresh');
		} else {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('admin/login');
			} else {
				redirect('admin', 'refresh');
			}
		}
	}

	public function check_database($password) {
		$username = $this->input->post('username');

		$result = $this->m_admin->login($username, $password);

		if ($result) {
			$sess_array = array();
			foreach ($result as $row) {
				$sess_array = array(
					'id' => $row->id,
					'username' => $row->username,
					'fullname' => $row->fullname
									);
				$this->session->set_userdata('loggedin', $sess_array);
			}
			return TRUE;
		} else {
			$this->form_validation->set_message('check_database', 'Username and Password is not Valid!');
			return false;
		}
	}

	function logout() {
		$this->session->keep_flashdata('pesan_logout');
		$this->session->unset_userdata('loggedin');
		//session_destroy();
		redirect('admin/login', 'refresh');
	}

}

?>
