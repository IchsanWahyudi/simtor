<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Kurir extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_kurir', '',TRUE);
		$this->load->model('m_barang', '',TRUE);
		$this->load->helper(array('form'));
	}

	public function index() {
		if ($this->session->userdata('loggedin')) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("class='form-error' title='", "'");
			$this->form_validation->set_message('is_unique', 'Permalink already exist.');
			$this->form_validation->set_rules('nama', 'Nama Kurir', 'trim|required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|');
			$this->form_validation->set_rules('password2', 'Retype Password', 'trim|required|xss_clean|matches[password]');
			
			if ($this->form_validation->run() == FALSE) {
				$data = array (
				  'pagetitle'   =>  "Sistem Monitoring Pengiriman Barang - Kurir",
				  'pos_parent'  =>  "kurir",
				  'title'       =>  "Manajemen Kurir",
				  'data'        =>  $this->m_kurir->getAll(), 
				  'subtitle'    =>  "Tambah Kurir",
				  'breadcrumb'  =>  array('Kurir','tambah kurir'),
				  'content'     =>  'admin/kurir/tambah'
				);
				
				$this->load->view('admin/template/page', $data); 

			}else {
				
				$data = array(
						'username'	=> $this->input->post('nama'),
						'password'	=> $this->input->post('password'),
					);
				
				$this->m_kurir->insert($data);
				$this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
					<p><b>Success!</b> Kurir atas nama '".$this->input->post('nama')."'' telah dibuat.<i class='fa fa-times'></i></p>
				</div>");
				redirect(base_url().'admin/kurir/', 'refresh');
			}
		} else {
			//If no session, redirect to login page
			redirect(base_url().'admin/login', 'refresh');
		}
	}
	
	public function ubah($id='') {
		if ($this->session->userdata('loggedin')) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters("class='form-error' title='", "'");
			$this->form_validation->set_message('is_unique', 'Permalink already exist.');
			$this->form_validation->set_rules('nama', 'Nama', 'trim|required|xss_clean');
			// $this->form_validation->set_rules('password2', 'Retype Password', 'trim|required|xss_clean|matches[password]');

			if ($this->form_validation->run() == FALSE) {
				$data = array (
				  'pagetitle'   =>  "Sistem Monitoring Pengiriman Barang - Kurir",
				  'pos_parent'  =>  "kurir",
				  'title'       =>  "Manajemen Kurir",
				  'subtitle'    =>  "Ubah Data Kurir",
				  'data_kurir'  =>  $this->m_kurir->getDetail($id),
				  'action'      =>  "<a class='button button-red' href='".base_url()."admin/kurir/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
				  'breadcrumb'  =>  array('Kurir','ubah'),
				  'content'     =>  'admin/kurir/ubah'
				);
				
				$this->load->view('admin/template/page', $data);

			}else {
				
				 if ($this->input->post('password') == '') {
				  $data = array(
					  'username'  => $this->input->post('nama'),
				  );
				} else {
				
				  $data = array(
					  'username'  => $this->input->post('nama'),
					  'password'  => $this->input->post('password'),
				   );
				}

				$this->m_kurir->update($id,$data);
				$this->session->set_flashdata("pesan", "<div class='alert alert-success'>
					<p><b>Ubah Data Sukses!</b>.<i class='fa fa-times'></i></p>
				</div>");
				
				redirect(base_url().'admin/kurir', 'refresh');
			}
		} else {
			//If no session, redirect to login page
			redirect(base_url().'admin/login', 'refresh');
		}
	}

	function hapus($id=''){
		if ($this->session->userdata('loggedin')) {
			$data_brg = $this->m_barang->getAll();

			foreach ($data_brg as $db) {
				if ($id == $db->id_courier) {
					$this->session->set_flashdata("pesan", "<div class='alert alert-warning'>
					<p><b>Sukses!</b> Kurir sedang melakukan pengiriman barang.<i class='fa fa-times'></i></p>
					</div>");
					redirect(base_url().'admin/kurir', 'refresh');
				}
			}
			
			$this->m_kurir->delete($id);		

			$this->session->set_flashdata("pesan", "<div class='alert alert-error'>
				<p><b>Sukses!</b> Data berhasi dihapus dari database.<i class='fa fa-times'></i></p>
			</div>");
			redirect(base_url().'admin/kurir', 'refresh');
		} else {
			//If no session, redirect to login page
			redirect(base_url().'admin/login', 'refresh');
		}
	}

}
