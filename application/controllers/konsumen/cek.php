<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cek extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form'));
        $this->load->model('m_barang', '', TRUE);
    }

    public function index() {
        if ($this->session->userdata('loginkonsumen')) {
            redirect('konsumen/track', 'refresh');
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
            $this->form_validation->set_rules('id', 'ID Barang', 'trim|required|xss_clean|callback_check_database');
            if ($this->form_validation->run() == FALSE) {
                 $data = array(
                    'pagetitle'     => "Sistem Monitoring Pengiriman Barang - Cek",
                    'content'       => 'content/konsumen/home',
                    'css'           => 'style-konsumen-home.css',
                );
                $this->load->view('content/template/page', $data);
            } else {
                redirect('konsumen', 'refresh');
            }
        }
    }

    public function check_database($id) {

        $result = $this->m_barang->tracking($id);

        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                    'id_goods'  =>  $row->id_goods,
                    'track_number'  => $row->track_number  
                );
                $this->session->set_userdata('loginkonsumen', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Nomor Pengiriman Tidak Valid!');
            return false;
        }
    }

    function logout() {
        $this->session->keep_flashdata('pesan_logout');
        $this->session->unset_userdata('loginkonsumen');
        redirect('konsumen', 'refresh');
    }


}

?>
