<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Track extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_barang', '', TRUE);
        $this->load->model('m_kurir', '', TRUE);
    }

    public function index($id='') {
        if ($this->session->userdata('loginkonsumen')) {
            $id_goods = $this->session->userdata('loginkonsumen')['id_goods'];
            $data = array(
            	'pagetitle'     => "Sistem Monitoring Pengiriman Barang - Track",
                'content'       => 'content/konsumen/track',
                'css'           => 'style-konsumen-track.css',
                'data_barang'   => $this->m_barang->getDetail($id_goods),
            );
            // print_r($data);
            // die();
            $this->load->view('content/template/page', $data);
        } else {
            redirect(base_url().'konsumen', 'refresh');
        }
    }

}