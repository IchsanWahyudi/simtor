<?php $this->load->view('content/template/header'); ?>

	<div class="container">
		<div class="cari">
			<?= form_open(base_url()."konsumen/cek", array("role" => "form"))?>
			<h3>Lacak Barang Anda</h3>
			<?= validation_errors(); ?>
            <?= $this->session->flashdata('pesan_logout'); ?>
			<div class="col-9">
				<div class="input-icon-l">
					<i class="fa fa-search"></i>
                    <input type='text' name='id' placeholder="Masukan Nomor Resi atau Id Barang Anda" />
				</div>
			</div>
			<div class="col-3">
				<div class="input-icon-r">
                    <input type='submit' value="Search"/>
				</div>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>

