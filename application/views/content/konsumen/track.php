<?php $this->load->view('content/template/header'); ?>

	<div class="container" style="margin-bottom:40px;background-color:#fff">
		<div class="row">
			<div class="row" style="position:relative;margin-bottom:25px">
				<div id="map"></div>
				<a href="#order" class='ui'>List Order</a>
			</div>
			<div class="col-12 " style="border:solid #eaeaea 1px">
				<div id="order" class="order">
				<?php foreach ($data_barang as $k) { ?>
					<table>
						<tr>
							<td>Nomor</td>
							<td><?= $k->track_number ?></td>
						</tr>
						<tr>
							<td>Nama Pengirim</td>
							<td><?= $k->sender ?></td>
						</tr>
						<tr>
							<td>Barang</td>
							<td><?= $k->goods ?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td><?= $k->address ?></td>
						</tr>
						<tr>
							<td>Nama Penerima</td>
							<td><?= $k->receiver ?></td>
						</tr>
						<tr>
							<td>Nama Kurir</td>
							<td><?= $k->username ?></td>
						</tr>
					</table>
				<?php } ?>
						<a href="<?= base_url() ?>konsumen/cek/logout">logout</a>
				</div>
			</div>
		</div>
	</div>

<style>

	#map{
		height: 500px;
		z-index: 1;
	}	

	@media(max-width: 460px){
		#map{
			height: 320px;
		}
	}

</style>

<script>
	$('a').click(function(){
		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 500);
		return false;
	});

	var id_courier = <?php foreach ($data_barang as $k) { echo $k->id_courier; }?>;
	var map;
	var marker;

	var KurirLocation = new Firebase('https://simtor.firebaseio.com/');
	var child_KurirLocation = KurirLocation.child('location');

		
		child_KurirLocation.child(id_courier).on('value', function(snapshot) {
			data = snapshot.val();
			// console.log(data);

			var lat = data.latitude;
			var lng = data.longitude;

			var latlng = new google.maps.LatLng(lat,lng);
			
			marker.setPosition(latlng);
			map.panTo(latlng);

		}, function (errorObject) {
				console.log("The read failed: " + errorObject.code);
			});


   function initMapKonsumen() {

		child_KurirLocation.child(id_courier).once('value', function(snapshot) {
			data = snapshot.val();
			// console.log('init',data);

			var lat = data.latitude;
			var lng = data.longitude;
			// var lat_destination = <?php foreach ($data_barang as $k) { echo $k->latitude; }?>;
			// var lng_destination = <?php foreach ($data_barang as $k) { echo $k->longitude; }?>;

			// console.log(lat_destination);
			// console.log(lng_destination);


			map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: lat, lng: lng},
				zoom: 16 
			});

			// marker_destination = new google.maps.Marker({
			// 	map: map,
			// 	position: {lat: lat_destination, lng: lng_destination}
			// });

			// var iconBase = 'http://icons.iconarchive.com/icons/iron-devil/ids-3d-icons-20/32/Truck-icon.png';
			// var iconHome = 'https://cdn4.iconfinder.com/data/icons/business-red/512/home_marker-128.png';
			var iconBase = 'http://static.gosunoob.com/img/gta_map_markers/truck_icon.png';
			
			marker = new google.maps.Marker({
				map: map,
				icon: iconBase,
				position: {lat: lat, lng: lng}
			});
		
		});

  }

</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMapKonsumen"
async defer></script>