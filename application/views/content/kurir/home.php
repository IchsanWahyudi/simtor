<div id="wrapper">
	<header>
		<div class="container-header">
			<div class="logo">
				<i class="fa fa-truck"></i>
				<h1>SIMTOR</h1>
				<h2>sistem monitoring pengiriman barang</h2>
			</div>
		</div>
	</header>

	<nav class="kurir">
		<ul>
			<li><i class="fa fa-user" style="margin-right:8px"></i><?= $this->session->userdata('loginkurir')['username'] ?></li>
			<a href="<?= base_url() ?>kurir/login/logout"><li><i class="fa fa-sign-out"></i>logout</li></a>
		</ul>
	</nav>

	<div class="kurir-mobile">
		<div class="toggle">
			<i class="fa fa-bars"></i>
		</div>
		<nav class="menu-mobile">
			<ul>
				<li><i class="fa fa-user" style="margin-right:8px"></i><?= $this->session->userdata('loginkurir')['username'] ?></li>
				<a href="<?= base_url() ?>kurir/login/logout"><li style="cursor:pointer"><i class="fa fa-sign-out"></i>logout</li></a>
			</ul>
		</nav>
	</div>

	<div class="row" style="position:relative;margin-bottom:25px">
		<div id="map"></div>
		<a href="#order" class='ui'>List Order</a>
	</div>

	<h2 style="text-align:center;margin-bottom:20px">Pengiriman Barang</h2>
	
	<!-- <div class="container" style="margin-top:20px;background-color:#fff"> -->
		<div class="row">
			<div class="col-12 wrap" style="border-top:solid rgb(82,99,125) 2px">
				<div id="order" class="order">
				<?php if ($data != '') { ?>
					<table>
						<thead>
							<tr>
								<th>Nomor Resi</th>
								<td><?=$data->track_number?></td>
							</tr>
							<tr>
								<th>Nama Pengirim</th>
								<td><?=$data->sender?></td>
							</tr>
							<tr>
								<th>Barang</th>
								<td><?=$data->goods?></td>
							</tr>
							<tr>
								<th>Alamat</th>
								<td><?=$data->address?></td>
							</tr>
							<tr>
								<th>Nama Penerima</th>
								<td><?=$data->receiver?></td>
							</tr>
							<tr>
								<th class='nosort'>Status Pengiriman</th>
								<td class="bt">
									<?php
										if ($data->status == 'sent') {
									   echo "<a href='".base_url()."kurir/home/status/".$data->id_goods."/on-the-way' class='button button-red' style='width:50%;text-align:center'><i class='fa fa-truck' style='margin-right:6px'></i>On The Way</a>";
									} else {
										echo "<a href='".base_url()."kurir/home/status/".$data->id_goods."/sent' class='button button-blue' style='width:50%;text-align:center'><i class='fa fa-check' style='margin-right:6px'></i>Sent</a>";
									}
									?>
								</td>
							</tr>
							</tr>
						</thead>
						<tbody>
							<input type="hidden" id="lat_value" value="<?= $data->latitude?>">
							<input type="hidden" id="lng_value" value="<?= $data->longitude?>">
							<?php
								$data_way = explode("|", $data->waypoint);
								$array = json_encode($data_way);
							?>
						</tbody>
					</table>
				<?php } else { ?>
					<p style="text-align:center">Tidak Ada Pengiriman Untuk Anda</p>
				<?php }  ?>
				</div>
			</div>
		</div>
	</div>
<!-- </div> -->

<style>

	#map{
		height: 500px;
		z-index: 1;
	}	

	@media(max-width: 460px){
		#map{
			height: 320px;
		}
	}

</style>

<script>
	var wayArray = <?= $array?>;

	$('a').click(function(){
		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 500);
		return false;
	});


	$('.toggle').on('click', function() {
		$('.menu-mobile').slideToggle();
	});

	var id_courier = <?= $this->session->userdata('loginkurir')['id_courier'] ?>;
	var map;
	var marker;
	var lat;
	var lng;
	var latitude;
	var longitude;

	var KurirLocation = new Firebase('https://simtor.firebaseio.com/');
	var child_KurirLocation = KurirLocation.child('location');

	if (navigator.geolocation) {

		navigator.geolocation.watchPosition(function(position) { 

		lat = position.coords["latitude"];
		lng = position.coords["longitude"];
		
		child_KurirLocation.child(id_courier).set({
			 'latitude': lat,
			 'longitude': lng
		});
		console.log(lat+"   "+lng);

		var latlng = new google.maps.LatLng(lat,lng);
		
		marker.setPosition(latlng);
		map.panTo(latlng);
		
	  },
	  function(error) {
		alert("Error: refresh web or check your connection", error);
	  },
	  {
		enableHighAccuracy: true,
		timeout: 5000,
		maximumAge: 0
	  }
	  );
	}  


   function initMapKurir() {

		var directionsDisplay = new google.maps.DirectionsRenderer({
  		    suppressMarkers: true,
      	// 	polylineOptions: {
       //  		strokeColor: "black"
     		// }
    	});
		var directionsService = new google.maps.DirectionsService;

		navigator.geolocation.getCurrentPosition(function(position) { 

			latitude = position.coords["latitude"];
			longitude = position.coords["longitude"];

			map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: latitude, lng: longitude},
				zoom: 18,
			});

			var iconBase = 'http://static.gosunoob.com/img/gta_map_markers/truck_icon.png';

			marker = new google.maps.Marker({
				map: map,
				icon: iconBase
			});

			directionsDisplay.setMap(map);

			calculateAndDisplayRoute(directionsService, directionsDisplay);

		  },
		  function(error) {
			alert("Error: refresh web ", error);
		  },
		  {
			enableHighAccuracy: true,
			timeout: 5000,
			maximumAge: 0
		  }
		  );

  }

  function calculateAndDisplayRoute(directionsService, directionsDisplay) {
		var lat = parseFloat($('#lat_value').val());
		var lng = parseFloat($('#lng_value').val());

		var waypts = [];

		for (var j = 0; j < wayArray.length; j++) {
			waypts.push({
				location: wayArray[j],
				stopover: true
			});
		}
		console.log(waypts);

	  directionsService.route({
		origin: {lat: -7.8197942, lng: 110.3837131},
		destination: {lat: lat, lng: lng},
		waypoints : waypts,
		travelMode: google.maps.TravelMode.DRIVING
	  }, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
		  directionsDisplay.setDirections(response);
		  // console.log(response);
		} else {
		  window.alert('Directions request failed due to ' + status);
		}
	  });
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMapKurir"
async defer></script>
