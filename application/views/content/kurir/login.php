<!DOCTYPE html>
<html>
<head>
    <title>SIMTOR Kurir</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fa/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery-ui.css"/>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/style-kurir-login.css'/>
</head>
<body class="login-block" style="background-image:url(<?= base_url()?>assets/images/bg.png)">
    <section id='login'>
        <div class='wrapper'>
            <div class="logo">
                <i class="fa fa-truck"></i><h1>SIMTOR</h1>
                <h2>sistem monitoring pengiriman barang</h2>
            </div>

            <div class='form'>
                <div class="form-body">
                    <div class='form-head'>
                        <h3>Silahkan Login</h3>
                    </div>
                    <?= form_open(base_url()."kurir/login", array("role" => "form"))?>
                    <?= validation_errors(); ?>
                    <?= $this->session->flashdata('pesan_logout'); ?>
                    <div class="form-group" style="margin-top:26px">
                        <label>Username :</label>
                        <div class="input-icon">
                            <i class="fa fa-user"></i>
                            <input type='text' name='username' autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password :</label>
                        <div class="input-icon">
                            <i class="fa fa-lock"></i>
                            <input type='password' name='password' />
                        </div>
                    </div>
                    <div class="form-group">
                        <input type='submit' value='Sign in'/>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </section>
</body>
</html>