<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('content/template/head'); ?>
</head>
<body>
        <main>
            <?php
            if (isset($content)) {
                    $this->load->view($content);
            }
            ?>
        </main>
</body>
</html>