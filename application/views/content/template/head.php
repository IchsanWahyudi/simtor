<title><?= $pagetitle; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fa/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery-ui.css"/>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/style-global.css'/> 
    <?php
        if (isset($css)) {
            echo '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/'.$css.'"/>
            ';
        }
    ?>
    <script src='https://cdn.firebase.com/js/client/2.2.1/firebase.js'></script>
    <script src="<?= base_url() ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>

	