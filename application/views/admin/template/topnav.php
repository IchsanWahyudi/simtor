<nav>
    <div class='logo-wrap'>
        <img src='<?= base_url() ?>assets/images/logo.png'/>
        <div class='text'>
            <h3 class='top'>
                <span>SIMTOR</span>
            </h3>
            <h4 class='bottom'>IMADMIN</h4>
        </div>
        <i class="fa fa-bars side-toggle tipb" title='Toggle Sidebar'></i>
    </div>
</nav>