<aside>
	<div class='profil'>
		<div class='foto' style='background-image:url(<?=base_url()?>assets/images/pp.jpg);'>
		</div>
		<div class='info'>
			<h4><?= $this->session->userdata('loggedin')['fullname'] ?></h4>
			<h5>Administrator</h5>
		</div>
		<div class='opsi'>
			<div class='button-group'>
				<i class="fa fa-cog dropdown-toggle"></i>
				<ul class='dropdown-menu right-pos'>
					<li class='head'>Profile Options</li>
					<!-- <li><a href='<?= base_url() ?>admin/admin'>Edit Profile</a></li> -->
					<li class='sparator'></li>
					<li><a href='<?= base_url() ?>admin/login/logout'>Logout</a></li>
				</ul>
			</div>
		</div>
	</div>
	<ul class='nav outer'>
				<li class='second <?= $pos_parent == 'kurir' ? "active" : ""; ?>'><a href="<?= base_url() ?>admin/kurir/"><i class="fa fa-user"></i>Kurir</a>
				</li>

				<li class='second <?= $pos_parent == 'barang' ? "active" : ""; ?>'><a><i class="fa fa-check-square-o"></i>Barang<i class='fa arrow'></i></a>
					<ul class='nav-second'>
						<li <?= isset($pos_child) && $pos_parent == 'barang' && $pos_child == 'daftar' ? "class='active'" : ""; ?>>
							<a href='<?= base_url() ?>admin/barang/'>Pengiriman Barang</a></li>
						<li <?= isset($pos_child) && $pos_parent == 'barang' && $pos_child == 'tambah' ? "class='active'" : ""; ?>>
							<a href='<?= base_url() ?>admin/barang/tambah'>Tambah Pengiriman Barang</a></li>
					</ul>
				</li>

				<!-- <li class='second <?= $pos_parent == 'tracking' ? "active" : ""; ?>'><a href='<?= base_url() ?>admin/tracking/'><i class="fa fa-map-marker"></i>Tracking Kurir</a>
				</li> -->
				
			</ul>
</aside>
