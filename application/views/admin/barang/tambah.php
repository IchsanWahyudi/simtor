<?= $this->session->flashdata('pesan') ?>
<div class='row'>

  <?= form_open_multipart(base_url()."admin/barang/tambah")?>
  <div class='col-1'>
    <div class='panel'>
      <div class='panel-body'>
       <div class='input-row'>
          <h5>Kurir :</h5>
          <select name="id_kurir">
            <option value="">Pilih Kurir</option>
            <?php
            foreach($data_kurir as $d){
              echo "<option text=".$d->id_courier." value=".$d->id_courier.">".$d->username."</option>";
            }
            ?>
          </select>
        </div>
        <div class='input-row'>
          <h5>Nomor Pengiriman :</h5>
          <input type='text' name="nomor_pengiriman" <?= form_error('nomor_pengiriman');?> value="<?= set_value('nomor_pengiriman'); ?>"/>
        </div>
        <div class='input-row'>
          <h5>Nama Pengirim :</h5>
          <input type='text' name="nama_pengirim" <?= form_error('nama_pengirim');?> value="<?= set_value('nama_pengirim'); ?>"/>
        </div>
        <div class='input-row'>
          <h5>Nama Penerima :</h5>
          <input type='text' name="nama_penerima" <?= form_error('nama_penerima');?> value="<?= set_value('nama_penerima'); ?>"/>
        </div>
        <div class='input-row'>
          <h5>Barang Yang Dikirim:</h5>
          <input type='text' name="barang" <?= form_error('barang');?> value="<?= set_value('barang'); ?>"/>
        </div>
        <div class='input-row'>
          <h5>Nomor Telepon :</h5>
          <input type='text' name="telepon" <?= form_error('telepon');?> value="<?= set_value('telepon'); ?>"/>
        </div>
        <div class='input-row submit'>
          <a href="<?= base_url()?>admin/barang/pilih_rute/"><button class='button button-blue' type='submit'>Submit & Pilih Jalan</button></a>
        </div>
      </div>
    </div>
  </div>

  <div class='col-3'>
    <div class='panel'>
      <div class='panel-body'>
        <div class='input-row'>
          <h5>Alamat Tujuan:</h5>
          <input type='text' id='alamat' name='alamat' <?= form_error('alamat');?> value="<?= set_value('alamat'); ?>"/>
        </div>
        <div class='input-row' style='height:500px'>
          <input id='lat' name="lat" type='hidden' <?= form_error('lat');?> value="<?= set_value('lat'); ?>">
          <input id='lng' name="lng" type='hidden' <?= form_error('lng');?> value="<?= set_value('lng'); ?>">
          <div id="map" style="height:100%"></div>
        </div>
      </div>
    </div>
  </div>

<?= form_close()?>

</div>

<script>


  function search_with_placeID(map){

    var input = document.getElementById('alamat');

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', function() {
      // infowindow.close();

      var place = autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }

      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(15);
      }

      // marker.setPlace({
      //           placeId: place.place_id,
      //           location: place.geometry.location,
      //         });
      // marker.setVisible(true);

      // infowindow.setContent('<div><strong>' + place.name + '</strong>'+ '<br>' + place.geometry.location +'<br>' place.formatted_address);
      // infowindow.open(map, marker);

      // var lat = document.getElementById('lat');
      // var lng = document.getElementById('lng');
      // lat.value = place.geometry.location.H;
      // lng.value = place.geometry.location.L;

      });

  }

  function click_drag_Marker(map) {
    var marker;

    google.maps.event.addListener(map, 'click', function(event) {
        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();
        var infowindow = new google.maps.InfoWindow();

        if (!marker) {
          marker = new google.maps.Marker({
            map: map,
            draggable : true,
            position : event.latLng
          });  
          
        }else{
          marker.setPosition(event.latLng);
        };


        google.maps.event.addListener(marker, 'click', function(event){
          infowindow.setContent('Latitude : ' + event.latLng.lat() + '<br/>Longitude : ' +event.latLng.lng());
          infowindow.open(map, marker);
          // this.setMap(null);
        });

        // lat.value = event.latLng.H;
        // lng.value = event.latLng.L;

        google.maps.event.addListener(marker, 'dragend', function(event){
          document.getElementById('lat').value = event.latLng.lat();
          document.getElementById('lng').value = event.latLng.lng();
          infowindow.setContent('Latitude : ' + event.latLng.lat() + '<br/>Longitude : ' +event.latLng.lng());
        });

    });

      var lat = document.getElementById('lat');
      var lng = document.getElementById('lng');

  }

  function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -7.7547707, lng: 110.3808057},
      zoom: 14,
      scrollwheel: false,
    });

    search_with_placeID(map);
    click_drag_Marker(map);

  } //end initMap

</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap"
async defer></script>


