 <?= $this->session->flashdata("pesan") ?>
<div class='row'>
	<div class='col-4'>
		<div class='panel'>
			<div class='panel-head'>
				<h5>Barang</h5>
			</div>
			<div class='panel-body'>
				<table class='bordered table-blue datatable'>
					<thead>
						<tr>
							<th>Nomor Pengiriman</th>
							<th>Nama Pengirim</th>
							<th>Barang Yang Dikirim</th>
							<th>Nama Penerima</th>
							<th>Alamat Tujuan</th>
							<th>Nomor Telepon</th>
							<th>Status</th>
							<!-- <th>Koordinat</th> -->
							<th>Kurir Yang Mengantar</th>
							<th class='nosort'>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($data as $p) {
							echo "<tr>
							<td>".$p->track_number."</td>
							<td>".$p->sender."</td>
							<td>".$p->goods."</td>
							<td>".$p->receiver."</td>
							<td>".$p->address."</td>
							<td>".$p->phone."</td>
							<td>".$p->status."</td>
							<td>".$p->username."</td>
							<td class='nowrap'>
								".anchor(base_url()."admin/barang/hapus/$p->id_goods", "<i class='fa fa-trash-o'></i>", "title='Delete' class='button button-red button-confirm tipb' style='width:100%'")."
							</td>
						</tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>