<?= $this->session->flashdata('pesan') ?>
<div class='row'>

  <div class='col-3'>
    <div class='panel'>
      <div class='panel-body'>
        <div class='input-row' >
          <div id="map" style="height:600px"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-1">
    <div class="panel">
      <div class="panel-body">
        <div class="input-row">
          <h5>Alamat Tujuan :</h5>
          <?php foreach ($data as $d) { ?>
          <input id='location' type='text' value="<?= $d->address ?>" readonly>
          <input id='val_lat' type='hidden' value="<?= $d->latitude?>">
          <input id='val_lng' type='hidden' value="<?= $d->longitude?>">
          <?php }?>
        </div>
      </div>
    </div>
  </div>

  <div class="col-1">
    <div class="panel">
      <div class="panel-body">
        <div class="input-row">
          <button class='button button-blue' style="width:100%" type="submit" onClick="window.location.reload()">Reset Map</button>
        </div>
      </div>
    </div>
  </div>

  <div class='col-1'>
    <div class='panel'>
      <div class='panel-body'>
        <div class='input-row' >
         <h5>Buat Waypoint (Alternatif Jalan) :</h5>
         <div id="titik_rute_add" style="font-size:9pt;font-family:'Roboto-Light';margin-top:6px"></div>
       </div>
       <div class='input-row submit'>
         <button id="tambah-rute" class='button button-green' type="submit" onclick="add_direction()">tambah alternatif</button>
       </div>
     </div>
   </div>
 </div>

 <!-- <div class='col-1'>
  <div class='panel'>
    <div class='panel-body'>
      <div class='input-row' >
        <h5>alternatif jalan: </h5>
        <div id="jalur_add" style="font-size:9pt;font-family:'Roboto-Light'"></div>
      </div>
      <div class='input-row submit'>
        <button id="lihat_seluruh_alternatif" class="button button-red" type="submit" onclick="create_all_direction()" >Lihat Seluruh Alternatif</button>
        <button class="button button-blue" type="submit" onclick="dijkstra()" >Cari Rute</button> 
      </div>
    </div>
  </div>
</div> -->

<?= form_open_multipart(base_url()."admin/barang/pilih_rute/".$data['0']->id_goods)?>
  <div class='col-1'>
    <div class='panel'>
      <div class='panel-body'>
      <div class='input-row' id="parent_jalur" name="fix_waypoint">
          <h5>jalur yang dipilih :</h5>
        </div>
        <div class='input-row submit'>
          <button class="button button-blue" type="submit" style="width:100%">Submit</button> 
      </div>
      </div>
    </div>
  </div>

<?= form_close();?>
</div> <!-- end row -->

<script>

  var titik_rute = [];
  var markers = [];
  var counter = 0;

  function click_drag_Marker(map) {

    google.maps.event.addListener(map, 'click', function(event) {

      marker = new google.maps.Marker({
        map: map,
        draggable: true,
        position: event.latLng,
        id: counter
      });

      counter ++;  
      add_waypoint(marker);

      markers.push(marker);

    });

  } 


  function close_titik_rute(delete_titik_rute,i){

    $(delete_titik_rute).parent().parent().remove();
    titik_rute.splice(i,1);

    var temp_titik_rute = [];
    var temp_markers = [];
    var bar = 0;

    markers[i].setMap(null);
    markers.splice(i,1);
    
    for (x in titik_rute){
      markers[x].id = bar;
      temp_markers.push(markers[x]);
      titik_rute[x].id = bar;
      temp_titik_rute.push(titik_rute[x]);
      bar++;
    }

    titik_rute = temp_titik_rute;
    markers = temp_markers;
    counter = titik_rute.length;

    var output = '';

    titik_rute.forEach(function(callback){
      output += "<div style='overflow-y:auto;padding:6px;background-color:rgb(243,243,243)'><div style='width:94%;float:left'><input style='border-radius:0;background-color:rgb(243,243,243);float:left' id="+callback.id+" data-id="+callback.id+" type='text' value="+ callback.lat +","+ callback.lng +" readonly></div><div style='width:6%;float:left;'><i onclick='close_titik_rute(this,"+callback.id+")' class='fa fa-close' style='cursor:pointer;position:relative;top:8px;left:5px'></i></div></div>";
      document.getElementById('titik_rute_add').innerHTML = output;
    }); 

  } 

  function close_jalur(delete_titik_jalur,i){

    $(delete_titik_jalur).parent().parent().remove();
    jalur.splice(i,1);
    // for (var j = 0; j < jalur.length ; j++) {
    //   output += "<div style='overflow-y:auto;border:solid #eaeaea 1px;margin-top:6px;padding:4px;background-color:rgb(243,243,243)'><div style='width:94%;float:left;'>Jalur "+ (j+1), j +
    //   "</div><div style='width:6%;float:left;'><i onclick='close_jalur(this,"+j+")' class='fa fa-close' style='cursor:pointer;position:relative;top:0px;'></i></div></div>";
    //   document.getElementById('jalur_add').innerHTML = output;
    // }; 

  } 

  function add_waypoint(marker){

    var latit = marker.position.lat();
    var longit = marker.position.lng();

    var aim = {lat: latit, lng: longit, id: marker.id};

    titik_rute[marker.id] = aim;

    var output = '';

    titik_rute.forEach(function(callback){
      output += "<div style='overflow-y:auto;padding:6px;background-color:rgb(243,243,243)'><div style='width:94%;float:left'><input style='border-radius:0;background-color:rgb(243,243,243);float:left' id="+callback.id+" type='text' value="+ callback.lat +","+ callback.lng +" readonly></div><div style='width:6%;float:left;'><i onclick='close_titik_rute(this,"+callback.id+")' class='fa fa-close' style='cursor:pointer;position:relative;top:8px;left:5px'></i></div></div>";
      document.getElementById('titik_rute_add').innerHTML = output;
    }); 

    google.maps.event.addListener(marker, 'dragend', function(event){

      titik_rute[this.id].lat = event.latLng.lat();
      titik_rute[this.id].lng = event.latLng.lng();
      
      var way_id = document.getElementById(this.id);
      way_id.value = titik_rute[this.id].lat +","+titik_rute[this.id].lng;

    });
    
  }

 
  var akhir_lat = $('#val_lat').val();
  var akhir_lng = $('#val_lng').val();
  var akhir = akhir_lat+","+akhir_lng;
  var jalur = [] ,gabung_jalan = ["-7.8197942,110.3837131",akhir];
  console.log("awal gabung_jalan",gabung_jalan);

  function add_direction(){  
    var jalan = [];

    for (var j = 0; j < titik_rute.length; j++) {
       jalan.push(titik_rute[j].lat+","+titik_rute[j].lng);  
    }
    // console.log('jalan',jalan);
    gabung_jalan = gabung_jalan.concat(jalan);
    console.log(gabung_jalan);

    var temp_hasil = [], hasil = [];
    for(var i = 0; i < gabung_jalan.length; i++) {
        if( temp_hasil[gabung_jalan[i]]) continue;
        temp_hasil[gabung_jalan[i]] = true;
        hasil.push(gabung_jalan[i]);
    }
    // hasil.concat(akhir);
    console.log("hasilnya",hasil);

    jalur.push(jalan);
    // console.log('jalur',jalur); 

    var output = '';
    
    for (var j = 0; j < jalur.length ; j++) {
      output += "<div style='overflow-y:auto;border:solid #eaeaea 1px;margin-top:6px;padding:4px;background-color:rgb(243,243,243)'><div style='width:94%;float:left;'>Jalur "+ (j+1) +
      "</div><div style='width:6%;float:left;'><i onclick='close_jalur(this,"+j+")' class='fa fa-close' style='cursor:pointer;position:relative;top:0px;'></i></div></div>";
      document.getElementById('jalur_add').innerHTML = output;
    }; 
    
  }
  
  // function create_direction(){

  //   var start = {lat: -7.759640062708658, lng: 110.3810977935791};

  //   var map = new google.maps.Map(document.getElementById('map'), {
  //     center: start,
  //     zoom: 14,
  //     scrollwheel: false,
  //   });

  //   var directionsDisplay = new google.maps.DirectionsRenderer({
  //     // suppressMarkers: true,
  //     polylineOptions: {
  //       strokeColor: "black"
  //     }
  //   });

  //   var directionsService = new google.maps.DirectionsService();

  //   directionsDisplay.setMap(map);

  //   var get_koor = document.getElementById('location').value;
  //   var get_alamat = document.getElementById('location').options[document.getElementById('location').selectedIndex].text;
  //   var pecar_koor = get_koor.split(',');
  //   var get_lat = document.getElementById('val_lat').value = parseFloat(pecar_koor[0]);
  //   var get_lng = document.getElementById('val_lng').value = parseFloat(pecar_koor[1]); 

  //   var end = {lat: get_lat, lng: get_lng};

  //   var waypoint = [];

  //   titik_rute.forEach(function(koor){
  //     way = new google.maps.LatLng(koor.lat, koor.lng)
  //     waypoint.push({
  //       location: way,
  //       stopover: true
  //     });
  //   });

  //   var request = {
  //     origin : start,
  //     destination : end,
  //     waypoints: waypoint,
  //     travelMode : google.maps.TravelMode.DRIVING
  //   };

  //   directionsService.route(request, function(response, status){
  //     if (status == google.maps.DirectionsStatus.OK) {

  //       // var dis = document.getElementById('distance');
  //       // var condis1 = response.routes[0].legs[0].distance.value;
  //       // var condis2 = response.routes[0].legs[1].distance.value;
  //       // // var condis = condis1 + condis2;
  //       // dis.value = condis1 +" meter, "+condis2+"meter";

  //       // var dur = document.getElementById('duration');
  //       // var condur1 = response.routes[0].legs[0].duration.value;
  //       // var condur2 = response.routes[0].legs[1].duration.value;
  //       // var condur = (condur1 + condur2) / 60;
  //       // dur.value = condur +" menit";

  //       directionsDisplay.setDirections(response);
  //       // console.log(response);
  //       // console.log("jarak A - B = " + response.routes[0].legs[0].distance.value);
  //       // console.log("jarak B - C = " + response.routes[0].legs[1].distance.value);
  //       // console.log("jarak C - D = " + response.routes[0].legs[2].distance.value);
        
  //     };
  //   });

  // } 


  var renderArray = [];
  var requestArray = [];
  var map;
  var temp_verteks = [];
  var colourArray = ['navy', 'grey', 'fuchsia', 'black', 'white', 'lime', 'maroon', 'purple', 'aqua', 'red', 'green', 'silver', 'olive', 'blue', 'yellow', 'teal'];


  function create_all_direction(){

        var start = {lat:-7.8197942, lng: 110.3837131};
        
        map = new google.maps.Map(document.getElementById('map'), {
          center: start,
          zoom: 14,
          scrollwheel: false,
        });

        var get_lat = parseFloat(document.getElementById('val_lat').value);
        var get_lng = parseFloat(document.getElementById('val_lng').value); 

        var end = {lat: get_lat, lng: get_lng};
        requestArray = [];

        for (var route in jalur){

            // var waypts = [];

            data = jalur[route];
            // console.log(data);

            // for (var j = 0; j < data.length; j++) {

            //     waypts.push({
            //         location: data[j],
            //         stopover: true
            //     });
            // }

            // console.log(waypts);

            var request = {
                origin: start,
                destination: end,
                // waypoints: waypts,
                travelMode: google.maps.TravelMode.DRIVING
            };

            requestArray.push({request});
        }

        process_request();
    }

    function process_request(){

        var directionsService = new google.maps.DirectionsService();

        var i = 0;

        function directionResults(result, status) {

            if (status == google.maps.DirectionsStatus.OK) {
                
                renderArray[i] = new google.maps.DirectionsRenderer();
                renderArray[i].setMap(map);

                renderArray[i].setDirections(result);
                renderArray[i].setOptions({
                    polylineOptions: {
                        strokeWeight: 4,
                        strokeOpacity: 0.8,
                        strokeColor: colourArray[i]
                    },
                });

                var array_verteks = [];
                var panjang = 0;
                var legs = result.routes[0].legs;
                var f = 0;
                console.log("result",result);

                for(var p=0; p<legs.length; p++) {
                    var lok_lat = legs[p].start_location.lat();
                    var lok_lng = legs[p].start_location.lng();
                    f = legs[p].distance.value;
                    // console.log('jarak',p+1);
                    
                    var verteks = {
                      'lat' : ''+lok_lat,
                      'lng' : ''+lok_lng,
                      'distance' : f
                    };
                    array_verteks.push(verteks);
                    
                }

                temp_verteks.push(array_verteks);
                
                console.log('verteks',array_verteks);
                console.log('temp_verteks',temp_verteks);

                // jarak.push(panjang);
                // console.log('jarak keseluruhan',jarak);

                next_request();
            }

        }

        function next_request(){
            i++;

            if(i >= requestArray.length){
                return;
            }

             directionsService.route(requestArray[i].request, directionResults);
        
        }

         directionsService.route(requestArray[i].request, directionResults);
         // click_drag_Marker();
    
  }

  // var jarak = [];
  // var temp_jalur;
  // // var japil;

  // function CalcRoute() {

  //     Array.prototype.min = function() {
  //       return Math.min.apply(null, this);
  //     };

  //    for (var e = 0; e < jarak.length; e++) {
  //      if (jarak[e] == jarak.min()) {
  //         temp_jalur = jalur[e];
  //         console.log(e);
  //      };
  //    };

  //    // for (var z = 0; z < temp_jalur.length; z++) {
  //    //   temp_jalur[z];
  //    //   japil = temp_jalur[z];
  //    // };
  //    //  console.log("japil",japil);
  //    //  document.getElementById('jalur_dipilih_way').value=japil;

  //     for (var z = 0; z < temp_jalur.length; z++) {
  //      temp_jalur[z];
  //      $('#parent_jalur').append('<input name="fix_waypoint[]" type="text" value="'+temp_jalur[z]+'" readonly>');
  //    };

  //     console.log("japil",temp_jalur);

  // }


  function initMap() {

    // var start = {lat: -7.759640062708658, lng: 110.3810977935791};
    var start = {lat:-7.8197942, lng: 110.3837131};

    map = new google.maps.Map(document.getElementById('map'), {
      center: start,
      zoom: 14,
      scrollwheel: false,
    });

    var get_lat = parseFloat(document.getElementById('val_lat').value);
    var get_lng = parseFloat(document.getElementById('val_lng').value); 
    var aim = {lat: get_lat, lng: get_lng};

    var markers = [
    ['lokasi awal',-7.8197942,110.3837131],
    ['lokasi tujuan',get_lat, get_lng]
    ];

    for (var m = 0; m < markers.length; m++) {
      var title = markers[m][0];
      var lat_marker = markers[m][1];
      var lng_marker = markers[m][2];

      lat_lng_marker = new google.maps.LatLng(lat_marker,lng_marker);

      var marker = new google.maps.Marker({
        map: map,
        title: title,
        position: lat_lng_marker
      });

      map.setCenter(marker.getPosition());

    };  

    click_drag_Marker(map);

  } //end initMap

</script>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap"
async defer></script>


