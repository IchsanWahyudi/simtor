<div class='row'>

	<div class='col-1'>
		<div class='panel'>
			<div class='panel-body'>
				<div class='input-row'>
					<button id="hitung" class='button button-red' type="submit" style="width:100%" onclick="dijkstra()">Perhitungan Dijkstra</button>
				</div>	
			</div>
		</div>
	</div>

</div>

<script>

var matrix = [];

function dijkstra() {

	matrix[0] = [];
	matrix[0][1] = 2;
	matrix[0][2] = 3;
	matrix[0][3] = 4;

	matrix[1] = [];
	matrix[1][2] = 1;
	matrix[1][4] = 6;
	
	matrix[2] = [];
	matrix[2][3] = 8;
	matrix[2][5] = 4;
	
	matrix[3] = [];
	matrix[3][5] = 7;
	matrix[3][6] = 13;

	matrix[4] = [];
	matrix[4][5] = 5;
	matrix[4][6] = 7;

	matrix[5] = [];
	matrix[5][6] = 9;

	// matrix[0] = [];
	// matrix[0][1] = 4;
	// matrix[0][4] = 2;
	// matrix[0][5] = 3;

	// matrix[1] = [];
	// matrix[1][2] = 13;
	// matrix[1][5] = 8;
	// matrix[1][6] = 7;
	
	// matrix[3] = [];
	// matrix[3][2] = 7;
	// matrix[3][6] = 5;

	// matrix[4] = [];
	// matrix[4][3] = 6;
	// matrix[4][5] = 1;

	// matrix[5] = [];
	// matrix[5][3] = 8;
	// matrix[5][6] = 4;

	// matrix[6] = [];
	// matrix[6][2] = 9;

	console.log('matrix awal',matrix);
	var hitung_dijkstra = [];
	var prev_hitung_dijkstra = []; // simpan nilai sebelumnya (satu baris)
	var already_boxed = []; // menandai jarak yang telah dipilih
	var i = 0; // index yang dipilih, nantinya sebagai from

	for(var x = 0; x <= matrix.length; x++){ // reset dijkstra dulu
		hitung_dijkstra[x] = {
			'distance'	: Infinity , //value
			'from'		: 0 //index
		}
		prev_hitung_dijkstra[x] = {
			'distance'	: Infinity , //value
			'from'		: 0 //index
		}
		// console.log('x',x);
		// console.log('hitung_dijkstra',hitung_dijkstra);
		// console.log('prev_hitung_dijkstra awal awal',prev_hitung_dijkstra);
	}

	while(i != matrix.length) {
		//masukin nilai" pada setiap rute sekaligus liat yg mana yg lebih pendek pada rute tujuan yg sama
		for(var j = 0; j <= matrix.length; j++) { //untuk setiap index kedua (titik tujuan)
			console.log('i = ',i,' j = ',j);
			console.log('matrix i & j awal',matrix[i][j]);
			if(i != j) { // index i sama index j sama maka tidak akan dihiraukan jaraknya
				if(typeof matrix[i][j] != 'undefined') { // jika ada jarak pada matrix jarak
					if(prev_hitung_dijkstra[i].distance == Infinity) prev_hitung_dijkstra[i].distance = 0; //set jarak infinity menjadi 0 agar dapat ditambahkan dengan nilai pada matrix jarak
					var distance = matrix[i][j] + prev_hitung_dijkstra[i].distance; // menjumlahkan nilai pada matrix jarak dengan nilai yang sudah dipilih (i)
					// console.log('distance',distance);
					if(distance < prev_hitung_dijkstra[j].distance) { // bandingkan jarak sebelumnya dengan jarak baru
						hitung_dijkstra[j] = { 'distance' : distance, 'from' : i}; // pembentukan distance dan from
						// console.log('hitung_dijkstra',hitung_dijkstra);
					}
					
				}
			};

		};
		// console.log('hitung_dijkstra',hitung_dijkstra);
		//ngecek dari satu baris yg mana yg paling kecil
		var min = 0;
		for(var k = 0; k <= matrix.length; k++) { 
			if(typeof already_boxed[k] == 'undefined') { //kalau belum dipilih / dikotakin
				console.log('matrix '+i+"->"+k, hitung_dijkstra[k].distance+" <= "+hitung_dijkstra[min].distance);
				if (hitung_dijkstra[k].distance <= hitung_dijkstra[min].distance) { // bandingkan 
					min = k; // index dari k untuk memilih nilai paling kecil diantara satu deret
					// console.log('yes',min);
				}
			}
		}

		already_boxed[min] = true; // proses pemilihan (dikotakin)
		i = min;
		console.log('i',i);
		console.log('matrix', hitung_dijkstra);
		prev_hitung_dijkstra = hitung_dijkstra;
	}

	hitung_dijkstra[0].distance = 0;
	route = [];
	index_route = matrix.length;

	route.push(index_route);
	// console.log('sementara',route);
	while(index_route!=0){
		// console.log('sebelum',index_route);	
		route.push(hitung_dijkstra[index_route].from);
		// console.log('route',route);	
		index_route = hitung_dijkstra[index_route].from;
		// console.log('sesudah',index_route);	
	}
	// console.log('hasil akhir 1',hitung_dijkstra);
	route = route.reverse();
	console.log('hasil akhir 2',route);

} //end function dijkstra

</script>