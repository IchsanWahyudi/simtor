<?= $this->session->flashdata('pesan') ?>
<div class='row'>

	<div class='col-3'>
		<div class='panel'>
			<div class='panel-body'>
				<div class='input-row' >
					<div id="map" style="height:600px"></div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-1">
		<div class="panel">
			<div class="panel-body">
				<div class="input-row">
					<h5>Alamat Tujuan :</h5>
					<?php foreach ($data as $d) { ?>
					<input id='location' type='text' value="<?= $d->address ?>" readonly>
					<input id='val_lat' type='hidden' value="<?= $d->latitude?>">
					<input id='val_lng' type='hidden' value="<?= $d->longitude?>">
					<?php }?>
				</div>
			</div>
		</div>
	</div>

 <!--  <div class="col-1">
	<div class="panel">
	  <div class="panel-body">
		<div class="input-row">
		  <button class='button button-blue' style="width:100%" type="submit" onClick="window.location.reload()">Reset Map</button>
		</div>
	  </div>
	</div>
</div> -->

<div class='col-1'>
	<div class='panel'>
		<div class='panel-body'>
			<div class='input-row' >
				<h5>Buat Titik Jalan  :</h5>
				<div id="titik_rute_add" style="font-size:9pt;font-family:'Roboto-Light';margin-top:6px"></div>
			</div>
			<div class='input-row submit'>
				<button id="pilih" class='button button-green' type="submit"  onclick="select_direction()">Pilih Rute Terpendek</button>
				<button id="hitung" class='button button-red' type="submit" onclick="create_direction()">Cari Rute</button>
			</div>
		</div>
	</div>
</div>


<?= form_open_multipart(base_url()."admin/barang/pilih_rute/".$data['0']->id_goods)?>
<div class='col-1'>
	<div class='panel'>
		<div class='panel-body'>
			<div class='input-row' id="parent_jalur" name="fix_waypoint">
				<h5>Simpan Koordinat :</h5>
			</div>
			<div class='input-row submit'>
				<button class="button button-blue" type="submit" style="width:100%">Submit</button> 
			</div>
		</div>
	</div>
</div>

<?= form_close();?>
</div> <!-- end row -->

<script>

$(document).ready(function(){
	$('#pilih').hide();

	$('#hitung').click(function(){
		$('#pilih').show();
	});
});


var titik_rute = [];
var markers = [];
var counter = 0;
var marker;
var map

function click_drag_Marker() {
	google.maps.event.addListener(map,'click', function(event) {

		marker = new google.maps.Marker({
			map: map,
			draggable: true,
			position: event.latLng,
			id: counter,
			animation: google.maps.Animation.DROP

		});

		counter ++;  
		
		markers.push(marker);

		// marker = markers[this.id];
		
		var infowindow = new google.maps.InfoWindow({
			content: ''+counter
		});

		infowindow.open(map, marker);

		// markers.addListener('click',function(){
		// 	infowindow.open(map, marker);
		// });

		add_waypoint();

	});
} 


function close_titik_rute(delete_titik_rute,i){

	$(delete_titik_rute).parent().parent().remove();
	titik_rute.splice(i,1);

	var temp_titik_rute = [];
	var temp_markers = [];
	var bar = 0;

	markers[i].setMap(null);
	markers.splice(i,1);

	for (x in titik_rute){
		markers[x].id = bar;
		temp_markers.push(markers[x]);
		titik_rute[x].id = bar;
		temp_titik_rute.push(titik_rute[x]);
		bar++;
	}

	titik_rute = temp_titik_rute;
	markers = temp_markers;
	counter = titik_rute.length;

	var output = '';

	titik_rute.forEach(function(callback){
	output += "<div style='overflow-y:auto;padding:6px;background-color:rgb(243,243,243)'><div style='width:94%;float:left'><input style='border-radius:0;background-color:rgb(243,243,243);float:left' id="+callback.id+" data-id="+callback.id+" type='text' value="+ callback.lat +","+ callback.lng +" readonly></div><div style='width:6%;float:left;'><i onclick='close_titik_rute(this,"+callback.id+")' class='fa fa-close' style='cursor:pointer;position:relative;top:8px;left:5px'></i></div></div>";
	document.getElementById('titik_rute_add').innerHTML = output;
	}); 

} 


function add_waypoint(){

	var latit = marker.position.lat();
	var longit = marker.position.lng();

	var aim = {lat: latit, lng: longit, id: marker.id};

	titik_rute[marker.id] = aim;

	// console.log(titik_rute);
	// console.log('aim',aim);

	var output = '';

	titik_rute.forEach(function(callback){
	output += "<div style='overflow-y:auto;padding:6px;background-color:rgb(243,243,243)'><div style='width:94%;float:left'><input style='border-radius:0;background-color:rgb(243,243,243);float:left' id="+callback.id+" type='text' value="+ callback.lat +","+ callback.lng +" readonly></div><div style='width:6%;float:left;'><i onclick='close_titik_rute(this,"+callback.id+")' class='fa fa-close' style='cursor:pointer;position:relative;top:8px;left:5px'></i></div></div>";
	document.getElementById('titik_rute_add').innerHTML = output;
	}); 

	google.maps.event.addListener(marker, 'dragend', function(event){

		titik_rute[this.id].lat = event.latLng.lat();
		titik_rute[this.id].lng = event.latLng.lng();

		var way_id = document.getElementById(this.id);
		way_id.value = titik_rute[this.id].lat +","+titik_rute[this.id].lng;

	});

}


var akhir_lat = $('#val_lat').val();
var akhir_lng = $('#val_lng').val();
var titik_array = [];
var matrix = [];
var renderArray = [];
var colourArray = ['navy', 'grey', 'fuchsia', 'black', 'white', 'lime', 'maroon', 'purple', 'aqua', 'red', 'green', 'silver', 'olive', 'blue', 'yellow', 'teal'];


function create_direction(){  

	var name_generator = 0;

	var verteks = {
		'lat'   : -7.8197942,
		'lng'   : 110.3837131,
		'name'  : name_generator++
	}
	titik_array.push(verteks);

	for(var j = 0; j < titik_rute.length; j++) {
		var verteks = {
			'lat' : titik_rute[j].lat,
			'lng' : titik_rute[j].lng,
			'name' : name_generator++
		};
		titik_array.push(verteks);  
	}

	var verteks = {
		'lat'   : Number(akhir_lat),
		'lng'   : Number(akhir_lng),
		'name'  : name_generator
	}
	titik_array.push(verteks);

	console.log(titik_array);

	var counter_requestArray = 0;
	var requestArray = [];

	//set semua request ke dalam request Array
	for(var i = 0; i < titik_array.length; i++) {
		for(var j = i+1; j < titik_array.length; j++) {
			// console.log('counter_requestArray',counter_requestArray);
			if (i != 0 || j != titik_array.length-1) {
				var request = {
					origin : {lat:titik_array[i].lat, lng: titik_array[i].lng},
					destination : {lat:titik_array[j].lat, lng: titik_array[j].lng},
					travelMode : google.maps.TravelMode.DRIVING,
				};
				requestArray[counter_requestArray++] = {
					'request' : request,
					'index1' : i,
					'index2' : j
				};
			};

		}; 
	};
	console.log('requestArray',requestArray);

	direction_request(requestArray);
}

function direction_request(requestArray){

	var directionsService = new google.maps.DirectionsService();

	var i = 0;
	// console.log('DR',requestArray.length);

	function directionResults(result, status) {

		if (status == google.maps.DirectionsStatus.OK) {
			renderArray[i] = new google.maps.DirectionsRenderer();
			renderArray[i].setMap(map);

			renderArray[i].setDirections(result);
			renderArray[i].setOptions({
				suppressMarkers: true,
				polylineOptions: {
					strokeWeight: 4,
					strokeOpacity: 0.8,
					strokeColor: colourArray[i]
				},
			});

			if(typeof matrix[requestArray[i].index1] == 'undefined') matrix[requestArray[i].index1] = [];
			console.log('distance matrix',matrix); 
			matrix[requestArray[i].index1][requestArray[i].index2] = result.routes[0].legs[0].distance.value;
			console.log('matrix '+requestArray[i].index1+" "+requestArray[i].index2,matrix[requestArray[i].index1][requestArray[i].index2]);
			next_request();
		}else{
			alert(status);
		}

	}

	function next_request(){
		i++;

		if(i >= requestArray.length){
			// console.log('X', i);
			dijkstra();
			return;
		}

		var delay = 400; //1 seconds
		setTimeout(function(){
			directionsService.route(requestArray[i].request, directionResults); // pembentukan jalur
		}, delay); 
			
	}

	directionsService.route(requestArray[i].request, directionResults);

}



function dijkstra() { 

	var dijkstra = [];
	var hitung_dijkstra = [];
	var prev_hitung_dijkstra = [];
	var already_boxed = [];
	var i = 0;
	var last_vertex = -1;

	for(var x = 0; x <= matrix.length; x++){ //reset dijkstra dulu
		hitung_dijkstra[x] = {
			'distance'	: Infinity , //value
			'from'		: 0 //index
		}
		prev_hitung_dijkstra[x] = {
			'distance'	: Infinity , //value
			'from'		: 0 //index
		}
	}

	while(i != matrix.length) {
		//masukin nilai" pada setiap rute sekaligus liat yg mana yg lebih pendek pada rute tujuan yg sama
		for(var j = 0; j <= matrix.length+1; j++) { //untuk setiap index kedua (titik tujuan)
			if(i != j) { // i sama j gak boleh sama karena jaraknya pasti 0 (gak kemana-mana)
				if(typeof matrix[i][j] != 'undefined') {
					if(prev_hitung_dijkstra[i].distance == Infinity) prev_hitung_dijkstra[i].distance = 0;
					var distance = matrix[i][j] + prev_hitung_dijkstra[i].distance; //kalau dy ada rutenya, ditambahkan
					if(distance < prev_hitung_dijkstra[j].distance || prev_hitung_dijkstra[i].distance == 0 ) {
						hitung_dijkstra[j] = { 'distance' : distance, 'from' : i}; //kalau nilai dari rute yang baru lebih kecil, itu yang dipake	
					}
					
				}
			};

		};
		// console.log('hitung_dijkstra',hitung_dijkstra);
		//ngecek dari satu baris yg mana yg paling kecil
		var min = 0;
		for(var k = 0; k <= matrix.length; k++) {
			if(typeof already_boxed[k] == 'undefined') { //kalau dy belum dikotakin
				// console.log('matrix '+i+"->"+k, hitung_dijkstra[k].distance+" <= "+hitung_dijkstra[min].distance);
				if (hitung_dijkstra[k].distance <= hitung_dijkstra[min].distance) {
					min = k;
					// console.log('yes',min);
				}
			}
		}

		already_boxed[min] = true;
		i = min;
		// console.log('i',i);
		console.log('matrix', hitung_dijkstra);
		prev_hitung_dijkstra = hitung_dijkstra;
	}

	hitung_dijkstra[0].distance = 0;
	route = [];
	index_route = matrix.length;
	route.push(index_route);
	while(index_route!=0){
		route.push(hitung_dijkstra[index_route].from);
		index_route = hitung_dijkstra[index_route].from;	
	}
	
	console.log('hasil akhir 1',hitung_dijkstra);
	console.log('hasil akhir 2',route);

} //end function dijkstra
 
var waypts = [];

function select_direction(){
  	var start;
	var end;
	map = new google.maps.Map(document.getElementById('map'), {
		center: start,
		zoom: 14,
		scrollwheel: false,
	});

	requestArray = [];
	
	route.forEach(function(ta){
		if (route.indexOf(ta) == 0) {
			start = {lat: titik_array[ta].lat, lng: titik_array[ta].lng}
		}else if(route.indexOf(ta) == route.length-1){
			end = {lat: titik_array[ta].lat, lng: titik_array[ta].lng}
		}else{
			data = titik_array[ta];
			// console.log(data);

			waypts.push({
				location: {lat: data.lat, lng: data.lng},
				stopover: true
			});
		}	
	});

	var request = {
		origin: start,
		destination: end,
		waypoints: waypts,
		travelMode: google.maps.TravelMode.DRIVING
	};

	requestArray.push({request});

	process_request();
}

function process_request(){

	var directionsService = new google.maps.DirectionsService();

	var i = 0;

	function directionResults(result, status) {

		if (status == google.maps.DirectionsStatus.OK) {

			renderArray[i] = new google.maps.DirectionsRenderer();
			renderArray[i].setMap(map);

			renderArray[i].setDirections(result);
			renderArray[i].setOptions({
				suppressMarkers: true,
			// 	polylineOptions: {
			// 		strokeWeight: 4,
			// 		strokeOpacity: 0.8,
			// 		strokeColor: colourArray[i]
			// 	},
			});
			console.log('waypts',waypts);
			var array_waypts = []; 
			// var array_waypts = $.map(waypts, function(value, index) {
			//     return [value];
			// });
			for(var z = 0; z < waypts.length; z++) {
				lat_waypts = ''+waypts[z].location.lat;
				lng_waypts = ''+waypts[z].location.lng;
				array_waypts.push(lat_waypts+","+lng_waypts);
				$('#parent_jalur').append('<input name="fix_waypoint[]" type="text" value="'+array_waypts[z]+'" readonly>');	
			};
			console.log('array_waypts',array_waypts); 

			next_request();
		}

	}

	function next_request(){
		i++;

		if(i >= requestArray.length){
			return;
		}

		directionsService.route(requestArray[i].request, directionResults);

	}

	directionsService.route(requestArray[i].request, directionResults);

} //end function proses_request


function initMap() {

	var get_lat = parseFloat(document.getElementById('val_lat').value);
	var get_lng = parseFloat(document.getElementById('val_lng').value); 
	// var aim = {lat: get_lat, lng: get_lng};

	map = new google.maps.Map(document.getElementById('map'), {
		// center: {lat:-7.8197942, lng: 110.3837131},
		center: {lat:get_lat, lng: get_lng},
		zoom: 14,
		scrollwheel: false,
	});

	var markers = [
	['lokasi awal',-7.8197942,110.3837131],
	['lokasi tujuan',get_lat, get_lng]
	];

	for(var m = 0; m < markers.length; m++) {
		var title = markers[m][0];
		var lat_marker = markers[m][1];
		var lng_marker = markers[m][2];

		lat_lng_marker = new google.maps.LatLng(lat_marker,lng_marker);

		marker = new google.maps.Marker({
			map: map,
			// title: title,
			position: lat_lng_marker
		});

		var infowindow = new google.maps.InfoWindow({
			content: title
		});

		infowindow.open(map, marker);

		map.setCenter(marker.getPosition());

	};

	click_drag_Marker();

} //end initMap

</script>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap"
async defer></script>


