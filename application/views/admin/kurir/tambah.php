<?= $this->session->flashdata('pesan') ?>
<div class='row'>

    <?= form_open_multipart(base_url()."admin/kurir/")?>
    <div class='col-2'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row' style="margin-top:10px;">
                    <h5>Nama :</h5>
                    <input type='text' name="nama" <?= form_error('nama');?> value="<?= set_value('nama'); ?>"/>
                </div>
                <div class='input-row'>
                    <h5>Password :</h5>
                    <input type='password' name='password' <?= form_error('password'); ?> value="<?= set_value('password'); ?>"/>
                </div>
                <div class='input-row'>
                    <h5>Retype Password :</h5>
                    <input type='password' name='password2' <?= form_error('password2'); ?>/>
                </div>
                <div class="input-row submit">
                        <button class='button button-blue' type='submit' value='upload'>Submit</button>
                </div>
            </div>
        </div>
    </div>
    <?= form_close()?>

    <div class="col-2">
        <div class="panel">
            <div class='panel-body'>
                <table class='bordered table-red datatable'>
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th class='nosort'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $u) {
                            echo "<tr>
                            <td>".$u->username."</td>
                            <td class='nowrap'>
                                ".anchor(base_url()."admin/kurir/ubah/$u->id_courier", "<i class='fa fa-pencil'></i>", "title='Ubah' class='button button-yellow'")." 
                                ".anchor(base_url()."admin/kurir/hapus/$u->id_courier", "<i class='fa fa-trash-o'></i>", "title='Hapus' class='button button-red button-confirm'")."
                            </td>
                        </tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


</div>