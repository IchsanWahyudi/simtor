<!DOCTYPE html>
<html>
<head>
    <title>SIMTOR</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/fa/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/jquery-ui.css"/>
    <link rel='stylesheet' type='text/css' href='<?= base_url() ?>assets/css/admin.css'/>
</head>
<body style='background-color: #313940;'>
    <section id='login'>
        <div class='wrapper'>
            <h1><span>Sistem Monitoring Pengiriman Barang</span></h1>
            <div class='form'>
                <div class='form-head'>
                    <h4>LOGIN</h4>
                </div>
                <?= form_open(base_url()."admin/login", array("role" => "form"))?>
                <?= validation_errors(); ?>
                <?= $this->session->flashdata('pesan_logout'); ?>
                <input type='text' name='username' placeholder='Username' autofocus/>
                <input type='password' name='password' placeholder='Password' />
                <input type='submit' value='Login'/>
                <?= form_close() ?>
            </div>
            <!-- <p>Powered by IMADMIN</p> -->
            <!-- <p style='margin-top: 0;'>Copyright &copy; 2013-<?= date('Y') ?> <a href='http://www.inncomedia.com' target='_blank'>INNCOMEDIA</a></p> -->
        </div>
    </section>
</body>
</html>