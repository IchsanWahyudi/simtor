<?php

Class m_kurir extends CI_Model {

        function login($username, $password) {
            $this->db->select('*');
            $this->db->from('courier');
            $this->db->where('username', $username);
            $this->db->where('password', $password);
            $this->db->limit(1);
            $query = $this->db->get();

            if ($query->num_rows() == 1) {
                return $query->result();
            } else {
                return false;
            }
        }

        function insert($data){
           $this->db->insert('courier',$data);
        }

        function getAll(){
            $this->db->select('*');
            $this->db->from('courier');
            $q  = $this->db->get();
            return $q->result();
        }

        function getDetail($id){
            $this->db->select('*');
            $this->db->where('id_courier',$id);
            $this->db->from('courier');
            // $q  = $this->db->get()->row();
            // return $q;
            $q  = $this->db->get();
            return $q->result();
        }

        function update($id,$data){
            $this->db->where('id_courier',$id);
            $this->db->update('courier',$data);
        }

        function delete($id){
            $this->db->where('id_courier',$id);
            $this->db->delete('courier');
        }

        // function Join(){
        //     $this->db->select('*');
        //     // $this->db->where('id_courier',$id);
        //     $this->db->from('courier');
        //     $this->db->join('goods', 'goods.id_courier = courier.id_courier', 'left');
        //     $query = $this->db->get();
        //     return $query->result();
        // }

}