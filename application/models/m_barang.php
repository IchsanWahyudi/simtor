<?php

Class m_barang extends CI_Model {

        function tracking($id) {
            $this->db->select('*');
            $this->db->from('goods');
            $this->db->where('track_number',$id);
            $this->db->limit(1);
            $query = $this->db->get();

            if ($query->num_rows() == 1) {
                return $query->result();
            } else {
                return false;
            }
        }

        // function getBarang($id){
        //     $this->db->select('*');
        //     $this->db->from('goods');
        //     $this->db->where('id_goods', $id);
        //     $q = $this->db->get();
        //     return $q->result();
        // }

        function getBarangKurir($id){
            $this->db->select('*');
            $this->db->from('goods');
            $this->db->where('id_courier', $id);
            $q = $this->db->get();
            return $q->row();
        }

        function insert($data){
           $this->db->insert('goods',$data);
           return $this->db->insert_id();
        }

        function insert_way($id, $data){
            $this->db->where('id_goods',$id);
            $this->db->update('goods',$data);
        }

        function getAll(){
            $this->db->select('*');
            $this->db->from('goods');
            $q  = $this->db->get();
            return $q->result();
        }

        function getDetail($id){
            $this->db->where('id_goods',$id);
            $this->db->select('*');
            $this->db->from('goods');
            $this->db->join('courier', 'courier.id_courier = goods.id_courier', 'left');
            $q  = $this->db->get();
            return $q->result();
        }

        function update($id,$data){
            $this->db->where('id_goods',$id);
            $this->db->update('goods',$data);
        }

        function update_status($id,$data){
            $this->db->where('id_courier',$id);
            $this->db->update('goods',$data);
        }

        function delete($id){
            $this->db->where('id_goods',$id);
            $this->db->delete('goods');
        }

        function Join(){
            $this->db->select('*');
            $this->db->from('goods');
            $this->db->join('courier', 'courier.id_courier = goods.id_courier', 'left');
            $query = $this->db->get();
            return $query->result();
        }
        
        // public $Join = "
        //     g.id_goods,
        //     g.track_number,
        //     g.sender,
        //     g.receiver,
        //     g.goods,
        //     g.phone,
        //     g.address,
        //     g.status,
        //     g.latitude,
        //     g.longitude,
        //     g.waypoint
        //     g.id_courier,
        // ";

}